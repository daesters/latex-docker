#!/bin/sh

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ROOTPATH="$SCRIPTPATH/.."
cd $ROOTPATH
pwd

docker run -a stdin -a stdout -i -t daesters/latex /bin/bash
