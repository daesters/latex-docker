# idea from aergus/latex

#we need debian:testing for inkscape 1.0
FROM debian:testing-slim 

LABEL maintainer="simon.daester@gmail.com"

ARG USER_NAME=latex
ARG USER_HOME=/home/latex
ARG USER_ID=1000
ARG USER_GECOS=LaTeX

RUN adduser \
  --home "$USER_HOME" \
  --uid $USER_ID \
  --gecos "$USER_GECOS" \
  --disabled-password \
  "$USER_NAME"

RUN apt-get update && apt-get install -y \
  # Whenever possible, ease later changes by sorting multi-line arguments alphanumerically
  # see https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#sort-multi-line-arguments
  apt-utils \
  git \
  inkscape \
  latexmk \
  make \
  texlive-base \
  texlive-fonts-recommended \
  # for fourier, newtxmath, .. It's big, I know
  texlive-fonts-extra \ 
  texlive-lang-german \
  texlive-latex-extra \
  texlive-latex-recommended \
  texlive-publishers \
  texlive-science \
  && \
  # Removing documentation packages *after* installing them is kind of hacky,
  # but it only adds some overhead while building the image.
  apt-get --purge remove -y .\*-doc$ && \
  # Remove more unnecessary stuff
  apt-get clean -y
  
