# README #

This is a LaTeX docker to have the most general setup for my LaTeX projects
I usually use texlive, together with scripts to convert svg2pdf.
The script is based on the [inkscape](https://https://inkscape.org/), which converts SVG files to PDF (and others)


### What is this repository for? ###

* Docker used for LaTeX compilation for my projects
* version 3.1.1

### How do I get set up? ###

* Basic docker stuff

### Contribution guidelines ###

* Almost always appreciate (see points below)
* Be nice
* Don't blow up code ;-)

### Who do I talk to? ###

* Blame: Me
* Docker problems: Docker community
* Faith issues: Thy Father in heaven
